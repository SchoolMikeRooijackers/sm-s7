# SMAN7 poll

De sman7 poll vind je [hier](http://goo.gl/forms/PuCXOF4S05).
De resultaten van de poll vin je [hier](https://docs.google.com/forms/d/1xkwR-rPOTlptxQQ1lpnDf-yCrqcfkyTTP_gGbhkAKPY/viewanalytics).

# Setup manual

Gebruik de SMTE4 installatie handleing ([SMTE4 installation manual](https://git.fhict.nl/smartmobile/sm-s4/raw/master/smte41/practica/SMTE4%20-%20installation%20manual.docx)).

# Android voorbeeld workspace

De voorbeeld android-workspace voor sman7 vind je [hier](https://git.fhict.nl/smartmobile/sm-s7-android-code/tree/master).